from django.urls import path

from . import views

urlpatterns = [
    path(
        '<str:username>',
        views.dashboard_view,
        name='user dashboard'
    ),
    path(
        '<str:username>/update_repos',
        views.update_repos_view,
        name='update repos'
    ),
    path(
        '<str:username>/create_tags',
        views.create_tags_view,
        name='create tags'
    ),
    path(
        '<str:username>/delete_tag',
        views.delete_tag_view,
        name='delete tag'
    ),
    path(
        '<str:username>/not_found',
        views.not_found_view,
        name='not found'
    ),
]
