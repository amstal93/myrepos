import requests
import logging
import logging.config
from .models import Repository


logging.config.fileConfig('logging.conf')
logger = logging.getLogger('GitHubRequester')


class GitHubRequester():
    """
    Class responsible for making requests to the GitHub API.

    GitHubRequester will always use a token to make requests when
    the requesting user is authenticated, as the GitHub API request
    limit for authenticated users is considerably higher.
    """

    def __init__(self, request):
        """
        Constructor method of the class.
        """

        self.request = request

        self.GITHUB_API_HOST = 'https://api.github.com'

        self.AUTHENTICATED_USER_ENDPOINT = self.GITHUB_API_HOST + '/user'
        self.AUTHENTICATED_REPOS_ENDPOINT = \
            self.AUTHENTICATED_USER_ENDPOINT + '/repos?affiliation=owner'

        self.USERS_ENDPOINT = self.GITHUB_API_HOST + '/users/<user>'
        self.REPOS_ENDPOINT = self.USERS_ENDPOINT + '/repos'

        self.STATISTICS_CONTRIBUTORS_ENDPOINT = self.GITHUB_API_HOST + \
            '/repos/<owner>/<repo>/stats/contributors'

        self.HEADERS = {'Authorization': 'token '}

    def request_user(self, username=None):
        """
        Method responsible for requesting a GitHub user.
        """

        logger.info(
            "Started requesting {username}'s user.".format(
                username=username
            )
        )

        if self.request.user.is_authenticated:

            social = self.request.user.social_auth.get(provider='github')
            token = social.extra_data['access_token']

            HEADERS = dict()
            HEADERS['Authorization'] = self.HEADERS['Authorization'] + token

            # Check if current user is requested user
            # Always use a token for authenticated users
            if username == self.request.user.username:

                user = requests.get(
                    self.AUTHENTICATED_USER_ENDPOINT,
                    headers=HEADERS
                )

            else:

                USERS_ENDPOINT = self.USERS_ENDPOINT.replace(
                    '<user>',
                    username
                )

                user = requests.get(
                    USERS_ENDPOINT,
                    headers=HEADERS
                )

        else:

            USERS_ENDPOINT = self.USERS_ENDPOINT.replace(
                '<user>',
                username
            )

            user = requests.get(
                USERS_ENDPOINT
            )

        user_response = dict()

        # If the request is successful, put the user information
        # in the dictionary
        if user.status_code == 200:

            user = user.json()

            user_response['login'] = user['login']
            user_response['avatar_url'] = user['avatar_url']
            user_response['name'] = user['name']
            user_response['location'] = user['location']
            user_response['public_repos'] = user['public_repos']
            user_response['html_url'] = user['html_url']
            user_response['followers'] = user['followers']
            user_response['following'] = user['following']
            user_response['followers_url'] = user['html_url'] + \
                '?tab=followers'
            user_response['following_url'] = user['html_url'] + \
                '?tab=following'
            user_response['repos_url'] = user['html_url'] + \
                '?tab=repositories'

        logger.info(
            "Finished requesting {username}'s user.".format(
                username=username
            )
        )

        return user_response

    def request_repositories(self, username=None):
        """
        Method responsible for requesting a user's repositories.
        """

        logger.info(
            "Started requesting {username}'s repositories.".format(
                username=username
            )
        )

        if self.request.user.is_authenticated:

            social = self.request.user.social_auth.get(provider='github')
            token = social.extra_data['access_token']

            HEADERS = dict()
            HEADERS['Authorization'] = self.HEADERS['Authorization'] + token

            # Check if current user is requested user
            # Always use a token for authenticated users
            if username == self.request.user.username:

                repositories = requests.get(
                    self.AUTHENTICATED_REPOS_ENDPOINT,
                    headers=HEADERS
                ).json()

            else:

                REPOS_ENDPOINT = self.REPOS_ENDPOINT.replace(
                    '<user>',
                    username
                )

                repositories = requests.get(
                    REPOS_ENDPOINT,
                    headers=HEADERS
                ).json()

        else:

            REPOS_ENDPOINT = self.REPOS_ENDPOINT.replace(
                '<user>',
                username
            )

            repositories = requests.get(
                REPOS_ENDPOINT
            ).json()

        index = 1
        repositories_response = list()
        for repository in repositories:

            repository_dict = dict()
            repository_dict['index'] = index
            repository_dict['id'] = repository['id']
            repository_dict['name'] = repository['name']
            repository_dict['full_name'] = repository['full_name']
            repository_dict['is_private'] = repository['private']
            repository_dict['is_fork'] = repository['fork']
            repository_dict['language'] = repository['language']
            repository_dict['description'] = repository['description']
            repository_dict['html_url'] = repository['html_url']

            # If the requesting user owns the requested repositories,
            # requests commit information from each repository
            if username == self.request.user.username:

                STATISTICS_CONTRIBUTORS_ENDPOINT = \
                    self.STATISTICS_CONTRIBUTORS_ENDPOINT.replace(
                        '<owner>',
                        repository['owner']['login']
                    ).replace(
                        '<repo>',
                        repository['name']
                    )

                contributors = requests.get(
                    STATISTICS_CONTRIBUTORS_ENDPOINT,
                    headers=HEADERS
                ).json()

                repository_dict['user_commits'] = None
                for contributor in contributors:

                    if contributor['author']['login'] == \
                            username:

                        repository_dict['user_commits'] = \
                            contributor['total']

                if not repository_dict['user_commits']:

                    repository_dict['user_commits'] = 0

            repositories_response.append(repository_dict)

            index += 1

        logger.info(
            "Finished requesting {username}'s repositories.".format(
                username=username
            )
        )

        return repositories_response
