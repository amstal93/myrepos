from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models import Sum
from django.shortcuts import render, redirect
from django.http import HttpResponse
from core.requester import GitHubRequester
from core.models import Repository, Tag

from django.http import HttpResponse

import json


def dashboard_view(request, username):
    """
    Method responsible for loading the user dashboard and making
    the requisitions and data processing required for each case.
    """

    requester = GitHubRequester(request)

    user = requester.request_user(username)

    if not user:

        return redirect(
            '/user/{username}/not_found'.format(
                username=username
            )
        )

    # Logic to run when user open your own dashboard
    if username == request.user.username:

        # If user doesn't have repositories in local database yet, create them
        if not request.user.repositories.all():

            repositories = requester.request_repositories(username)

            index = 1
            bulk_list = list()
            for repository in repositories:

                repository.pop('index', None)
                repository['owner'] = request.user

                bulk_list.append(Repository(**repository))

                repository['index'] = index
                index += 1

            Repository.objects.bulk_create(bulk_list)

        else:

            serialized_repositories = \
                json.loads(
                    serializers.serialize(
                        "json",
                        request.user.repositories.all()
                    )
                )

            repositories = list()
            for serialized_repository in serialized_repositories:

                tags_queryset = request.user.tags.all().filter(
                    repository=serialized_repository['pk']
                )

                tags = list()
                for tag in tags_queryset:
                    tags.append(tag.text)

                serialized_repository['fields']['tags'] = tags
                serialized_repository['fields']['id'] = \
                    serialized_repository['pk']
                repositories.append(serialized_repository['fields'])

            repositories = sorted(
                repositories,
                key=lambda k: k['name'].lower()
            )

            index = 1
            for repository in repositories:
                repository['index'] = index
                index += 1

        user_model = User.objects.get(username=username)
        user['commits'] = \
            user_model.repositories.aggregate(
                Sum('user_commits')
            )['user_commits__sum']

    # Logic to run when user doesn't open your own dashboard
    else:

        repositories = requester.request_repositories(username)

        if User.objects.filter(username=username).exists():

            for repository in repositories:

                tags_queryset = Tag.objects.all().filter(
                    repository=repository['id']
                )

                tags = list()
                for tag in tags_queryset:
                    tags.append(tag.text)

                repository['tags'] = tags

    return render(
        request,
        'dash.html',
        {
            'user': user,
            'repositories': repositories
        }
    )


def update_repos_view(request, username):
    """
    Method responsible for updating user repositories.
    """

    if username == request.user.username:

        requester = GitHubRequester(request)

        repositories = requester.request_repositories(username)

        repository_ids_list = list()
        for repository in repositories:

            repository.pop('index', None)

            filtered_repository = \
                request.user.repositories.all().filter(
                    id=repository['id']
                )

            # Update exiting repository
            if filtered_repository.exists():
                filtered_repository.update(**repository)

            # Create GitHub created repository
            else:
                repository['owner'] = request.user
                Repository.objects.create(**repository)

            repository_ids_list.append(repository['id'])

        # Delete GitHub deleted repositories
        request.user.repositories.exclude(
            id__in=repository_ids_list
        ).delete()

        return redirect(
            '/user/{username}'.format(
                username=username
            )
        )

    else:

        return redirect('/')


def create_tags_view(request, username):
    """
    Method responsible for creating tags for a repository.
    """

    if username == request.user.username:

        repository = request.GET.get('repository')
        tags = request.GET.getlist('tags')

        repository_model = Repository.objects.get(id=repository)

        for tag in tags:

            Tag.objects.create(
                owner=request.user,
                repository=repository_model,
                text=tag
            )

        return HttpResponse(status=200)  # OK

    else:

        return HttpResponse(status=401)  # Unauthorized


def delete_tag_view(request, username):
    """
    Method responsible for deleting a tag to a repository.
    """

    if username == request.user.username:

        repository = request.GET.get('repository')
        tag = request.GET.get('tag')

        repository_model = Repository.objects.get(id=repository)

        tag_to_delete = Tag.objects.filter(
            owner=request.user,
            repository=repository_model,
            text=tag
        )
        tag_to_delete.delete()

        return HttpResponse(status=200)  # OK

    else:

        return HttpResponse(status=401)  # Unauthorized


def not_found_view(request, username):
    """
    Method responsible for loading user not found page.
    """

    return render(
        request,
        'not_found.html',
        {
            'username': username
        }
    )
