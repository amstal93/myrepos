import pytest
from django.test import Client, RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from core.models import Repository, Tag
from core.requester import GitHubRequester
from social_django.models import UserSocialAuth


@pytest.fixture
def client():
    yield Client()

@pytest.mark.django_db
def test_repository_create(client):
    user = User.objects.create(username='test_user')
    user.set_password('123')
    user.save()

    repository_data = {
        "owner": user,
        "name": "repo",
        "full_name": "user/repo",
        "is_private": False,
        "is_fork": False,
        "description": "repository description",
        "language": "Python",
        "html_url": "https://github.com/user/repo",
        "user_commits": 85
    }

    repository = Repository.objects.create(**repository_data)

    assert repository is not None
    assert repository.__str__() == repository_data["full_name"]

@pytest.mark.django_db
def test_tag_create(client):
    user = User.objects.create(username='test_user')
    user.set_password('123')
    user.save()

    repository_data = {
        "owner": user,
        "name": "repo",
        "full_name": "user/repo",
        "is_private": False,
        "is_fork": False,
        "description": "repository description",
        "language": "Python",
        "html_url": "https://github.com/user/repo",
        "user_commits": 85
    }

    repository = Repository.objects.create(**repository_data)

    assert repository is not None

    tag_data = {
        "owner": user,
        "repository": repository,
        "text": "Python"
    }

    tag = Tag.objects.create(**tag_data)

    assert tag is not None
    assert tag.__str__() == tag_data["repository"].full_name + " - " + \
        tag_data["text"]

def test_github_requester_instantiate(client):
    request = RequestFactory().get('/')
    requester = GitHubRequester(request)

    assert requester is not None

@pytest.mark.django_db
def test_github_request_user_authenticated_same_user(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='mateusdemorais')
    user.set_password('123')
    user.save()

    social_auth_data = {
        "user": user,
        "provider": "github",
        "uid": 48456754,
        "extra_data": {
            "auth_time": 1566701945,
            "id": 48456754,
            "expires": None,
            "login": "mateusdemorais",
            "access_token": "2348234y2hr23hr238423h4234324234h2342343",
            "token_type": "bearer"
        }
    }

    social_auth = UserSocialAuth.objects.create(**social_auth_data)

    request.user = user

    requester = GitHubRequester(request)
    assert requester is not None

    response = requester.request_user('mateusdemorais')
    assert response is not None

@pytest.mark.django_db
def test_github_request_user_authenticated_different_user(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='mateusdemorais')
    user.set_password('123')
    user.save()

    social_auth_data = {
        "user": user,
        "provider": "github",
        "uid": 48456754,
        "extra_data": {
            "auth_time": 1566701945,
            "id": 48456754,
            "expires": None,
            "login": "mateusdemorais",
            "access_token": "2348234y2hr23hr238423h4234324234h2342343",
            "token_type": "bearer"
        }
    }

    social_auth = UserSocialAuth.objects.create(**social_auth_data)

    request.user = user

    requester = GitHubRequester(request)
    assert requester is not None

    response = requester.request_user('different_user')
    assert response is not None

@pytest.mark.django_db
def test_github_request_user_not_authenticated(client):
    request = RequestFactory().get('/')
    request.user = AnonymousUser()

    requester = GitHubRequester(request)
    assert requester is not None

    response = requester.request_user('user')
    assert response is not None

@pytest.mark.django_db
def test_github_request_repositories_not_authenticated(client):
    request = RequestFactory().get('/')
    request.user = AnonymousUser()

    requester = GitHubRequester(request)
    assert requester is not None

    response = requester.request_repositories('user')
    assert response is not None

def test_not_found_page(client):
    response = client.get('/user/username/not_found')
    assert response.status_code == 200

@pytest.mark.django_db
def test_create_tag_different_user(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='username')
    user.set_password('123')
    user.save()

    request.user = user

    response = client.get('/user/different_username/create_tags?repository=123&tags=Python')
    assert response.status_code == 401

@pytest.mark.django_db
def test_delete_tag_different_user(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='username')
    user.set_password('123')
    user.save()

    request.user = user

    response = client.get('/user/different_username/delete_tag?repository=123&tag=Python')
    assert response.status_code == 401

@pytest.mark.django_db
def test_open_dashboard_user_not_found(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='test_user')
    user.set_password('123')
    user.save()

    social_auth_data = {
        "user": user,
        "provider": "github",
        "uid": 48456754,
        "extra_data": {
            "auth_time": 1566701945,
            "id": 48456754,
            "expires": None,
            "login": "test_user",
            "access_token": "2348234y2hr23hr238423h4234324234h2342343",
            "token_type": "bearer"
        }
    }

    social_auth = UserSocialAuth.objects.create(**social_auth_data)

    request.user = user

    response = client.get('/user/test_user_not_found')
    assert response.status_code == 302

@pytest.mark.django_db
def test_update_repos_different_user(client):
    request = RequestFactory().get('/')

    user = User.objects.create(username='username')
    user.set_password('123')
    user.save()

    request.user = user

    response = client.get('/user/different_username/update_repos')
    assert response.status_code == 302
