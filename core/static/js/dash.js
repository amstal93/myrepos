var tagsList = [];
var tagsSearch = [];
var mouseOverTagText = null;
var mouseOverIcon = '<i class="fas fa-times-circle"></i> ';
var mouseLeaveIcon = '<i class="fas fa-check-circle"></i> ';
var iconChanged = false;
var alertedEmptyTag = false;
var alertedNoTag = false;
var alertedTagExists = false;
var repository = null;
var tag = null;

$(document).ready(function(){

    $('[data-toggle-2="tooltip"]').tooltip()


    // Tag deletion
    // Run when mouse enter in a existing tag element
    $("span[to_select='existingTag']").mouseenter(function() {
        if(iconChanged == false) {
            mouseOverTagText = $(this).html();
            $(this).html(mouseOverIcon.concat(mouseOverTagText));
            mouseOverTagText = null;
            iconChanged = true;
        }
    });

    // Run when mouse leaves a existing tag element
    $("span[to_select='existingTag']").mouseleave(function() {
        if(iconChanged == true) {
            mouseOverTagText = $(this).html().replace(mouseOverIcon, '');
            $(this).html(mouseOverTagText);
            mouseOverTagText = null;
            iconChanged = false;
        }
    });

    // Run when delete tag button is pressed
    $("span[to_select='existingTag']").click(function(){
        $("#confirmationDeleteTagModalBody").html(
            'Would you like to delete the tag "[TAG]" from repository "[REPOSITORY]"?'
        )

        var repositoryName = $(this).attr("repository_name");
        repository = $(this).attr("repository");
        tag = $(this).html().replace(mouseOverIcon, '');

        var newConfirmationDeleteTagModalBody = $(
            "#confirmationDeleteTagModalBody"
        ).html().replace(
            "[TAG]",
            tag
        ).replace(
            "[REPOSITORY]",
            repositoryName
        )

        $("#confirmationDeleteTagModalBody").html(
            newConfirmationDeleteTagModalBody
        )
    });

    // Run when delete tag modal is hidden
    $('#confirmationDeleteTag').on('hidden.bs.modal', function () {
        repository = null;
        tag = null;
    });

    // Run when confirm tag deletion button is pressed
    $("#confirmTagDeletion").click(function(){

        $.ajax({
            type: "GET",
            url: $(location).attr('href').concat("/delete_tag"),
            data: {
                repository: repository,
                tag: tag
            },
            traditional: true,
            success: function (response) {
                window.location.replace($(location).attr('href'));
            },
            error: function () {
                // Do something
            }
        });
    });


    // Search
    // Make contains case insensitive
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function(elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    // Run when enter is pressed focused in tag text input
    $('#searchTag').keypress(function (e) {
        var key = e.which;

        // The enter key code
        if(key == 13) {
            $("#goSearch").click();
        }
    });

    // Run when tag text input is focused
    $('#searchTag').focus(function() {
        if(alertedEmptyTag == true) {
            $("#emptyTagAlert").remove();
            alertedEmptyTag = false;
        }
        if(alertedTagExists == true) {
            $("#tagExistsAlert").remove();
            alertedTagExists = false;
        }
    });

    // Run when tag is added to search
    $("#goSearch").click(function(){
        var searchTag = $("#searchTag").val();

        // Tag is not empty
        if(searchTag) {

            var tagNotExists = true;
            for(var k=0; k < tagsSearch.length; k++) {
                if(searchTag.toUpperCase() == tagsSearch[k].toUpperCase()) {
                    tagNotExists = false;
                }
            }

            // Tag not exists
            if(tagNotExists) {
                var tagElement = "<span id=\"searchTag-[TAG]\" to_select=\"searchTagElement\" class=\"badge badge-pill badge-warning cursor\" style=\"margin: .25rem; font-size: 14px;\"><i class=\"fas fa-check-circle\"></i> [TAG]</span>";
                var searchTagElement = tagElement.replace("[TAG]", searchTag).replace("[TAG]", searchTag);

                // Add tag to the list of tags and to the page
                tagsSearch.push(searchTag);
                $("#tagsCreationSpaceSearch").append(searchTagElement);
                $("#searchTag").val("");
                $("#searchTag").focus();

                // Run when mouse enter in a tag element
                $("span[to_select='searchTagElement']").mouseenter(function() {
                    if(iconChanged == false) {
                        mouseOverTagText = $(this).html().replace(mouseLeaveIcon, '');
                        $(this).html(mouseOverIcon.concat(mouseOverTagText));
                        mouseOverTagText = null;
                        iconChanged = true;
                    }
                });

                // Run when mouse leaves a tag element
                $("span[to_select='searchTagElement']").mouseleave(function() {
                    if(iconChanged == true) {
                        mouseOverTagText = $(this).html().replace(mouseOverIcon, '');
                        $(this).html(mouseLeaveIcon.concat(mouseOverTagText));
                        mouseOverTagText = null;
                        iconChanged = false;
                    }
                });

                // Run when a tag element is clicked
                $("span[to_select='searchTagElement']").click(function(){
                    var index = tagsSearch.indexOf($(this).html().replace(mouseOverIcon, ''));
                    if (index > -1) {
                        tagsSearch.splice(index, 1);
                    }
                    $(this).remove();
                    mouseOverTagText = null;
                    iconChanged = false;
                    $("#searchTag").focus();

                    if(tagsSearch.length > 0) {

                        $("tbody").hide()

                        for(var i=0; i < tagsSearch.length; i++) {
                            var tBodySearch = $(
                                "span:contains('[TAG]')".replace(
                                    "[TAG]",
                                    tagsSearch[i]
                                )
                            ).parent().parent().parent().parent()

                            if(tBodySearch.length > 1) {
                                for(var j=0; j < tBodySearch.length; j++) {
                                    if($(tBodySearch[j]).prop("tagName") == 'TBODY') {
                                        $(tBodySearch[j]).show();
                                    }
                                }
                            }
                        }
                    }
                    else {
                        $("tbody").show()
                    }

                });

                $("tbody").hide()

                for(var i=0; i < tagsSearch.length; i++) {
                    var tBodySearch = $(
                        "span:contains('[TAG]')".replace(
                            "[TAG]",
                            tagsSearch[i]
                        )
                    ).parent().parent().parent().parent()

                    if(tBodySearch.length > 1) {
                        for(var j=0; j < tBodySearch.length; j++) {
                            if($(tBodySearch[j]).prop("tagName") == 'TBODY') {
                                $(tBodySearch[j]).show();
                            }
                        }
                    }
                }
            }

            // Tag exists
            else {

                if(alertedTagExists == false) {
                    var alertElement = '<div id="tagExistsAlert" class="alert alert-warning" role="alert">This tag has already been added!</div>';
                    $("#searchAlertSpace").append(alertElement);
                    alertedTagExists = true;
                }
            }
        }

        // Tag is empty
        else {

            if(alertedEmptyTag == false) {
                var alertElement = '<div id="emptyTagAlert" class="alert alert-warning" role="alert">Enter a value!</div>';
                $("#searchAlertSpace").append(alertElement);
                alertedEmptyTag = true;
            }
        }
    });


    // Add tags
    // Run when tags modal is shown
    $('#createTag').on('shown.bs.modal', function () {
        $('#newTag').focus();
    });

    // Run when tags modal is hidden
    $('#createTag').on('hidden.bs.modal', function () {
        tagsList = [];
        $("span[to_select='newTagElement']").remove();
        repository = null;
    });

    // Run when enter is pressed focused in tag text input
    $('#newTag').keypress(function (e) {
        var key = e.which;

        // The enter key code
        if(key == 13) {
            $("#addTag").click();
        }
    });

    // Run when tag text input is focused
    $('#newTag').focus(function() {
        if(alertedEmptyTag == true) {
            $("#emptyTagAlert").remove();
            alertedEmptyTag = false;
        }
        if(alertedNoTag == true) {
            $("#noTagAlert").remove();
            alertedNoTag = false;
        }
    });

    // Run when tag is added
    $("#addTag").click(function(){

        var newTag = $("#newTag").val();

        // Tag is not empty
        if(newTag) {

            var tagElement = "<span id=\"newTag-[TAG]\" to_select=\"newTagElement\" class=\"badge badge-pill badge-warning cursor\" style=\"margin: .25rem; font-size: 14px;\"><i class=\"fas fa-check-circle\"></i> [TAG]</span>";
            var newTagElement = tagElement.replace("[TAG]", newTag).replace("[TAG]", newTag);

            // Add tag to the list of tags and to the page
            tagsList.push(newTag);
            $("#tagsCreationSpace").append(newTagElement);
            $("#newTag").val("");
            $("#newTag").focus();

            // Run when mouse enter in a tag element
            $("span[to_select='newTagElement']").mouseenter(function() {
                if(iconChanged == false) {
                    mouseOverTagText = $(this).html().replace(mouseLeaveIcon, '');
                    $(this).html(mouseOverIcon.concat(mouseOverTagText));
                    mouseOverTagText = null;
                    iconChanged = true;
                }
            });

            // Run when mouse leaves a tag element
            $("span[to_select='newTagElement']").mouseleave(function() {
                if(iconChanged == true) {
                    mouseOverTagText = $(this).html().replace(mouseOverIcon, '');
                    $(this).html(mouseLeaveIcon.concat(mouseOverTagText));
                    mouseOverTagText = null;
                    iconChanged = false;
                }
            });

            // Run when a tag element is clicked
            $("span[to_select='newTagElement']").click(function(){
                var index = tagsList.indexOf($(this).html().replace(mouseOverIcon, ''));
                if (index > -1) {
                    tagsList.splice(index, 1);
                }
                $(this).remove();
                mouseOverTagText = null;
                iconChanged = false;
                $("#newTag").focus();
            });
        }

        // Tag is empty
        else {

            if(alertedEmptyTag == false) {
                var alertElement = '<div id="emptyTagAlert" class="alert alert-warning" role="alert">Enter a value!</div>';
                $("#tagsAlertSpace").append(alertElement);
                alertedEmptyTag = true;
            }
        }
    });

    // Run when open tags modal button is pressed
    $("button[name='buttonOpenModalTags']").click(function(){
        repository = $(this).attr("repository");
    });

    // Run when confirm tag creation button is pressed
    $("#confirmTagsCreation").click(function(){

        if(tagsList.length > 0) {

            $.ajax({
                type: "GET",
                url: $(location).attr('href').concat("/create_tags"),
                data: {
                    repository: repository,
                    tags: tagsList
                },
                traditional: true,
                success: function (response) {
                    window.location.replace($(location).attr('href'));
                },
                error: function () {
                    // Do something
                }
            });
        }
        else {
            if(alertedNoTag == false) {
                var alertElement = '<div id="noTagAlert" class="alert alert-warning" role="alert">You need to enter some tag to confirm!</div>';
                $("#tagsAlertSpace").append(alertElement);
                alertedNoTag = true;
            }
        }

    });
});
