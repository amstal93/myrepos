import pytest
from django.test import Client
from django.contrib.auth.models import User


@pytest.fixture
def client():
    yield Client()

def test_homepage_unlogged(client):
    response = client.get('/')
    assert response.status_code == 200

@pytest.mark.django_db
def test_homepage_user_redirected_to_profile(client):
    user = User.objects.create(username='test_user')
    user.set_password('123')
    user.save()

    client.force_login(user)

    response = client.get('/authentication/login')

    assert response.url == '/'
    assert response.status_code == 302

    response = client.get('/')

    assert response.url == '/user/test_user'
    assert response.status_code == 302
