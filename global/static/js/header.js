$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    // Run when enter is pressed focused in search text input
    $('#searchedUser').keypress(function (e) {
        var key = e.which;

        // The enter key code
        if(key == 13) {
            $("#searchButton").click();
        }
    });

    // Run when search button is pressed
    $("#searchButton").click(function() {
        var searchedUser = $("#searchedUser").val();

        if(searchedUser) {
            window.location.replace(
                $(location).attr('origin').concat("/user/").concat(searchedUser)
            );
            $("#searchedUser").val(searchedUser);
        }
        else {
            $('#searchedUser').focus();
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});
