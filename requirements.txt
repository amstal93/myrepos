Django==2.2.4
psycopg2
gunicorn
social-auth-app-django
ipython
pytest-django
pytest-cov
pycodestyle
