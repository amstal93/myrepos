# My Repos

| Ambiente        | Branch  | Status do CI | Cobertura de testes |
|:---------------:|:-------:|:------------:|:-------------------:|
| Produção        | master  |[![pipeline status](https://gitlab.com/mateusdemorais/myrepos/badges/master/build.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/master)  |[![coverage report](https://gitlab.com/mateusdemorais/myrepos/badges/master/coverage.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/master)  |
| Homologação     | staging |[![pipeline status](https://gitlab.com/mateusdemorais/myrepos/badges/staging/build.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/staging)|[![coverage report](https://gitlab.com/mateusdemorais/myrepos/badges/staging/coverage.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/staging)|
| Desenvolvimento | dev     |[![pipeline status](https://gitlab.com/mateusdemorais/myrepos/badges/dev/build.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/dev)        |[![coverage report](https://gitlab.com/mateusdemorais/myrepos/badges/dev/coverage.svg)](https://gitlab.com/mateusdemorais/myrepos/commits/dev)        |

* **Link para sistema no ambiente de produção:** [HTTPS://myrepos.ml/](https://myrepos.ml/)
* **Link para sistema no ambiente de homologação:** [http://homologation.myrepos.ml/](http://homologation.myrepos.ml/)

## Execução

Para executar a aplicação localmente, basta:

1. Clonar o repositório

```
git clone git@gitlab.com:mateusdemorais/myrepos.git
```

2. Executar por meio do `docker-compose` (neste caso, no modo *detached*):

```
docker-compose up -d
```

Obs.: Como a aplicação utiliza autenticação via *GitHub*, foi necessário criar um *OAuth App* para cada um dos ambientes, já que é necessário definir uma *callback URL* para a realização do *login*. Sendo assim, ao autenticar-se em desenvolvimento, homologação e produção, deverão ser fornecidas autorizações separadamente para cada um dos ambientes.

***OAuth Apps criados no GitHub:***

* My Repos
* My Repos - Homologation
* My Repos - Development

Para parar a execução e limpar o ambiente:

```
docker-compose down
```

## Testes

1. Para executar os testes, primeiramente, deve-se entrar no container da aplicação:

```
docker-compose exec myrepos bash
```

2. Após isso, para rodar uma análise utilizando o *Style Guide* PEP 8, deve-se executar o seguinte comando (nenhuma saída no terminal = *OK*):

```
pycodestyle --exclude='manage.py, myrepos/settings.py, myrepos/gitlab-ci-settings.py, */tests.py, */migrations' .
```

3. Por fim, para executar os testes:

```
pytest --ds=myrepos.gitlab-ci-settings --cov=authentication --cov=core --cov=global --disable-warnings
```

## Estrutura de arquivos

O sistema está sendo dividido em 3 *apps*:

* ***Authentication:*** *App* que lida com implementações relacionadas à autenticação.
* ***Core:*** *App* que compõe as implementações principais do sistema, que são relativas às requisições à *API* do *GitHub*, ao *dashboard*, *models* e manipulações de dados diversas.
* ***Global:*** *App* que compõe implementações relacionadas ao sistema como um todo, como o *template* que serve como base para todas as páginas e a barra de navegação.

## Implementações principais

### Login via GitHub utilizando o protocolo OAuth2

![login github](documentation/gif/login-github.gif)

### Adição de tags

![add tags](documentation/gif/add-tags.gif)

### Remoção de tags

![delete tags](documentation/gif/delete-tags.gif)

### Buscar repositórios

![search](documentation/gif/search.gif)

### Buscar usuários (tanto cadastrados no sistema quanto não cadastrados)

![search user](documentation/gif/search-user.gif)

### Redirects para páginas do GitHub

Por meio do *My Repos* é possível ir para páginas específicas do GitHub ao clicar em alguns elementos dos perfis (páginas são abertas em novas abas).

![links](documentation/img/links.png)

### Avisos em situações específicas

![alert-1](documentation/img/alert-1.png)

![alert-2](documentation/img/alert-2.png)

![alert-3](documentation/img/alert-3.png)

## Pontos a serem observados

### Kubernetes

O sistema conta com arquivos de configuração (*manifest files*) para realização de deploy de toda a infraestrutura necessária por meio do **Kubernetes**.

* Os *manifest files* se encontram no diretório `/kubernetes-manifest-files`, estando divididos por meio dos subdiretórios `/prod` e `/staging`.
* Para a aplicação *Django*, está sendo utilizado um objeto do tipo *Horizontal Pod Autoscaler*, que automaticamente cria e destroi contêineres com base no quão a aplicação está sendo requisitada (utilização de CPU).

### Google Cloud Platform

A aplicação está sendo disponibilizada por meio da *Google Cloud Platform*, onde se encontra criado o *cluster* do sistema com todos os objetos *Kubernetes*.

![cluster](documentation/img/cluster.png)

![deployments](documentation/img/deployments.png)

![services](documentation/img/services.png)

![configs](documentation/img/configs.png)

### Keel

O *Keel* pretende ser um serviço simples, robusto e que roda em background para atualizar automaticamente as cargas de trabalho do *Kubernetes*: ele surge para automatizar o processo de atualização e implantação de novas imagens *Docker* quando estas estiverem disponíveis.

Ou seja, sem a sua utilização, torna-se necessário que se acesse manualmente o *cluster* e atualize-se os contêineres. Já com a sua utilização, esse processo se torna automatizado, pois este serviço detecta quando uma nova imagem é gerada pela integração contínua do *GitLab*, e faz com que seja iniciado o processo de atualização por meio do *Kubernetes*.

**Repositório do *Keel*:** [https://github.com/keel-hq/keel](https://github.com/keel-hq/keel)

### HTTPS

O sistema está sendo servido (no ambiente de produção) por meio do protocolo *HTTPS* com a utilização de um certificado *TLS* gerado pelo *Let's Encrypt*. Com a configuração efetuada, o certificado será renovado automaticamente antes de expirar.

![https](documentation/img/https.png)

### Responsividade

O sistema foi desenvolvendo utilizando o *Bootstrap* e funciona bem em diferentes resoluções de tela.

![responsive](documentation/img/responsive.jpg)

![responsive](documentation/gif/responsive.gif)

### Domínio

O sistema está sendo disponibilizado por meio de domínios próprios (*https://myrepos.ml/* e *http://homologation.myrepos.ml/*).

### Deploy contínuo

O sistema conta com configuração para realização automática de *deploys*.

#### Estratégia de deploy

1. Uma nova versão da aplicação é desenvolvida;
2. Essa versão é enviada para uma *branch* do repositório;
3. A integração contínua do *GitLab* é iniciada e executa os devidos testes;
4. Caso os testes não apontem nenhuma falha, ao ser executada na *branch* de homologação do repositório (*staging*), a integração contínua inicia o processo de gerar uma nova imagem *Docker* com a nova versão da aplicação e enviar para o *registry* de contêineres do *GitLab* com a tag *staging*;
5. Caso os testes não apontem nenhuma falha, ao ser executada na *branch* principal do repositório (*master*), a integração contínua inicia o processo de gerar uma nova imagem *Docker* com a nova versão da aplicação e enviar para o *registry* de contêineres do *GitLab* com a tag *master*;
6. Sendo assim, o *registry* de contêineres do *GitLab* recebe uma nova imagem com a nova versão da aplicação;
7. O *Keel* detecta que existe uma nova imagem no *registry* do *GitLab* e, inicia o processo de atualização através do *Kubernetes*;
8. O *Kubernetes* atualiza gradualmente os contêineres com a nova versão (uma nova imagem com a *tag staging* atualizará os contêineres de produção e uma nova imagem com a *tag master* atualizará os contêineres do homologação).

Sendo assim, a integração contínua, em qualquer *branch* que não seja a *master* e nem a *staging*, só seguiria até o passo 3.

### Integração contínua

O sistema está utilizando o *GitLab CI*.

![ci](documentation/img/ci.png)

### Registry de contêineres

O sistema está se servindo de imagens guardadas no *registry* de contêineres do *GitLab*.

![registry](documentation/img/registry.png)
